import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Declaro la constante del IVA
		final double IVA = 0.21;
		
		// Pido el precio del producto
		System.out.println("Introduce el precio de un producto");
		double precio = teclado.nextDouble();
		
		// Calculo el IVA
		double precioIVA = precio*IVA;
		
		// Sumo el precio de producto + su IVA
		double precioFinal = precio + precioIVA;
		
		// Muestro el precio final del producto
		System.out.println("El precio final del producto es = " + precioFinal);
	}

}
