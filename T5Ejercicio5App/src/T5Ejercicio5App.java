import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pido que el usuario introduzca un n�mero
		System.out.println("Introduce un n�mero");
		// Recojo los datos
		int num = teclado.nextInt();
		
		// Creo un if que condicione que si el numero es divisible entre dos
		if (num%2 == 0) {
			// Muestre esto
			System.out.println(num + " es divisible entre 2");
		}
		// Si no es divisible entre 2
		else {
			// muestre esto
			System.out.println(num + " no es divisible entre 2");
		}
	}

}
