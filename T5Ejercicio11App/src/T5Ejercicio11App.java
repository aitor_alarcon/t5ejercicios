import java.util.Scanner;

public class T5Ejercicio11App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// le pido al usuario que introduzca un dia de la semana
		System.out.println("Introduce un d�a de la semana");
		// recojo el dia
		String dia = teclado.next();
		
		// Creo un switch que depende del d�a introducido dir� si es laboral o no
		switch (dia) {
		case "Lunes":
			// Muestro si es laboral o no
			System.out.println(dia + " si es un d�a laboral");
			break;
		
		case "Martes":
			// Muestro si es laboral o no
			System.out.println(dia + " si es un d�a laboral");
			break;
			
		case "Miercoles":
			// Muestro si es laboral o no
			System.out.println(dia + " si es un d�a laboral");
			break;
			
		case "Jueves":
			// Muestro si es laboral o no
			System.out.println(dia + " si es un d�a laboral");
			break;
			
		case "Viernes":
			// Muestro si es laboral o no
			System.out.println(dia + " si es un d�a laboral");
			break;
			
		case "Sabado":
			// Muestro si es laboral o no
			System.out.println(dia + " no es un d�a laboral");
			break;
			
		case "Domingo":
			// Muestro si es laboral o no
			System.out.println(dia + " no es un d�a laboral");
			break;

		default:
			// Muestro por defecto que no ha puesto ning�n d�a
			System.out.println("No has introducido ning�n d�a");
			break;
		}
	}

}
