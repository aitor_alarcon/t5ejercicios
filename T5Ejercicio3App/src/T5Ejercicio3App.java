import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo un string para que recoja el nombre del usuario
		String nombre = JOptionPane.showInputDialog("Introduce tu nombre");
				
		// Muestro el mensaje de bienvenida
		JOptionPane.showMessageDialog(null, "Bienvenid@ " + nombre);
	}

}
