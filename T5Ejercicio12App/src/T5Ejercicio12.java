import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Creo el string con la contraseņa
		String contraseņa = "megustajava";
		
		// Creo el string intentoContraseņa para usarlo con la condicion del doWhile
		String intentoContraseņa = "aaa";
		
		// Creo el contador de los intentos
		int intentos = 0;
		
		// Creo el bucle DoWhile
		do {
			// Le pido al usuario que introduzca la contraseņa
			System.out.println("Introduce la contraseņa");
			// guardo el intento
			intentoContraseņa = teclado.next();
			
			// Creo un if que compare las dos contraseņas si son iguales
			if (intentoContraseņa.equals(contraseņa)) {
				// muestra enhorabuena
				System.out.println("Enhorabuena");
			}
			else {
				// Si falla le dice que vuelva a intentarlo
				System.out.println("Vuelve a intentarlo");
			}
			
			// Incremento los intentos
			intentos++;
			
			// El bucle tiene que repetirse mientras la contraseņa introducida sea diferente a la creada
			// y mientras el contador no llegue a 3
		} while (!intentoContraseņa.equals(contraseņa) && intentos != 3);
	}

}
