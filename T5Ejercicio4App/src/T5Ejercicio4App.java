import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Declaro la variable global PI
		final double PI = 3.1416;
		
		// Pregunto que radio tiene el circulo
		System.out.println("Que radio tiene el circulo?");
		// Recojo los datos
		int radio = teclado.nextInt();
		
		// Calculo el area
		double area = PI * Math.pow(radio, 2);
		
		// Muestro el resultado
		System.out.println("El area del circulo es = " + area);
	}

}
