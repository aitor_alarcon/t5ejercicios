/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio9App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creo un bucle for que muestre los numeros del 1 al 100
		for (int num = 1; num <= 100; num++) {
			// Creo un if para que muestre solo los numeros divisibles
			// entre 2 y 3
			if(num % 2 == 0 || num % 3 == 0) {
				// Muestro el numero
				System.out.println(num);
			}
		}
	}

}
