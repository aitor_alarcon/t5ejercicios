/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio7App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// inicializo un numero a 1
		int num = 1;
		
		// Creo el bucle while
		while(num <= 100) {
			// Muestro el numero del 1 al 100
			System.out.println(num);
			// Incremento el numero por cada pasada del programa
			num++;
		}
	}

}
