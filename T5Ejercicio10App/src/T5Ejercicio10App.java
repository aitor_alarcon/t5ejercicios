import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio10App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Pido al usuario cuantas vendas va a realizar
		System.out.println("Cuantas vendas vas a realizar");
		// Guardo el n�mero
		int numVendas = teclado.nextInt();
		
		// Inicializo la variable precioTotal para poder sumar m�s tarde
		int precioTotal = 0;
		
		// Creo el bucle for que le pregunte el precio de las vendas
		// tantas veces como vendas ha dicho el usuario que va a realizar
		for (int vendas = 1; vendas <= numVendas; vendas++) {
			// Le pido el precio de la venda
			System.out.println("Introduce el precio de la venda " + vendas);
			// Recojo el precio
			int precioVenda = teclado.nextInt();
			// Voy sumando los precios al total
			precioTotal = precioTotal + precioVenda;
		}
		
		// Muestro el precio total de las vendas
		System.out.println("Este es el precio total de todas las vendas = " + precioTotal + "�");
	}

}
