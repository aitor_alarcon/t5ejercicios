/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//inicializo las dos variables
		int num1 = 21, num2 = 65;
		
		// Creo el condicional de si el num1 es mayor que el num2
		if(num1 > num2) {
			// muestra esto
			System.out.println("El numero " + num1 + " es mayor que " + num2);
		}
		// Si el num2 es mayor que num1
		else if(num2 > num1) {
			// muestra esto
			System.out.println("El numero " + num2 + " es mayor que " + num1);
		}
		// Si los dos numeros son iguales
		else {
			// muestra esto
			System.out.println("El numero " + num1 + " y el numero " + num2 + " son iguales");
		}
	}

}
