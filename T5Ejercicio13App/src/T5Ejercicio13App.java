import java.util.Scanner;

/**
 * 
 */

/**
 * @author dobla
 *
 */
public class T5Ejercicio13App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Declaro el Scanner
		Scanner teclado = new Scanner(System.in);
		
		// Le pido al usuario que introduzca dos numeros
		System.out.println("Introduce dos numeros");
		System.out.println("N�mero 1");
		int num1 = teclado.nextInt();
		System.out.println("N�mero 2");
		int num2 = teclado.nextInt();
		
		// Le pido que me introduzca un signo aritmetico
		System.out.println("Que quieres hacer con ellos? + (suma), - (resta), * (multiplicar) \n , / (dividir), ^ (potencia), % (resto)");
		String signoAritmetico = teclado.next();
		
		// Creo un condicional dependiendo del signo aritmetico introducido
		if(signoAritmetico.equals("+")) {
			// si es + sumar� los dos numeros
			int suma = num1 + num2;
			// muestra la suma
			System.out.println(suma);
		}
		else if(signoAritmetico.equals("-")) {
			// si es - restar� los dos numeros
			int resta = num1 - num2;
			// muestra la resta
			System.out.println(resta);
		}
		else if(signoAritmetico.equals("*")) {
			// si es * multiplicar� 
			int multiplicacion = num1 * num2;
			// muestra la multiplicacion
			System.out.println(multiplicacion);
		}
		else if(signoAritmetico.equals("/")) {
			// si es / dividir�
			int division = num1 / num2;
			// muestra la division
			System.out.println(division);
		}
		else if(signoAritmetico.equals("^")) {
			// si es ^ har� la potencia
			int potencia = (int)Math.pow(num1, num2);
			// muetra la potencia
			System.out.println(potencia);
		}
		else {
			// si no es ninguno de los anteriores har� el resto
			int resto = num1 % num2;
			// muestra el resto
			System.out.println(resto);
		}
	}

}
